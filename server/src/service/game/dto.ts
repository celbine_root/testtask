import { ArgsType, Field, ObjectType } from 'type-graphql';
import { IsNumber, Max, Min } from 'class-validator';
import { GameEnum } from '../../type';
import { GameEntity, UserEntity } from '../../database/entity';

@ArgsType()
export class DiceBetDtoInput {
  @IsNumber({
    maxDecimalPlaces: 2,
    allowNaN: false,
    allowInfinity: false,
  }, {
    message: 'Введите корректное число, до двух знаков после запятой',
  })
  @Min(1, {
    message: 'Минимальная сумма ставки: $constraint1 р.',
  })
  @Max(5, {
    message: 'Максимальная сумма ставки: $constraint1 р.',
  })
  @Field()
  amount: number;
}

@ObjectType()
export class ValueGameDtoOutput extends GameEntity{
  @Field()
  value: number;
}

@ArgsType()
export class GetGameDtoInput extends GameEntity{
  @IsNumber()
  @Field()
  id: number;
}

export class CreateGameDtoInput {
  constructor(
    public game: GameEnum,
    public gameId: number,
    public amount: number,
    public winAmount: number,
    public user: UserEntity
  ) {
  }
}
