import { Express } from 'express';
import * as express from 'express';
import { createSchema } from './graphql/schema';
import * as bodyParser from 'body-parser';
import * as http from 'http';
import { ApolloServer } from './graphql/apollo.server';

export class ServerRun {
  private readonly app: Express;
  private readonly port: number;

  constructor(port: number) {
    this.port = port;
    this.app = express();
  }

  async start() {
    try {
      const schema = await createSchema();

      this.app.use(bodyParser.json());
      this.app.use(bodyParser.urlencoded({extended: true}));

      const apolloServer = (new ApolloServer()).get(schema);
      apolloServer.applyMiddleware({app: this.app});


      const server = http.createServer(this.app);
      apolloServer.installSubscriptionHandlers(server);

      server.listen(this.port, () => {
        console.log(`Http server ready on port [${this.port}]`);
      });
    } catch (e) {
      console.error(e);
    }
  }
}
