import {
  DiceBetDtoInput,
  GameService, GetGameDtoInput, ValueGameDtoOutput
} from '../../service';
import { Args, Authorized, Ctx, Mutation, Query, Resolver, Root, Subscription } from 'type-graphql';
import { GameEntity, UserEntity } from '../../database/entity';
import { UserContext } from '../../type';
import redis from '../../database/redis.connection';
import { getCustomRepository } from 'typeorm';
import { UserRepository } from '../../database/repository';

@Resolver((of) => GameEntity)
export class GameResolver {
  service: GameService;

  constructor() {
    this.service = new GameService();
  }

  @Authorized()
  @Mutation((returns) => String)
  public async betOnDice(
    @Args() {amount}: DiceBetDtoInput,
    @Ctx() ctx: UserContext,
  ) {
    const game = await this.service.betOnDice(ctx, amount);

    setTimeout(async () => {
      const user = await getCustomRepository(UserRepository).getUserById(ctx.user.id);
      await Promise.all([
        redis.publish('onGame', JSON.stringify({id: ctx.user.id, game})),
        redis.publish('onBalance', JSON.stringify({id: ctx.user.id, balance: user.balance})),
      ]);
    }, 5 * 1000);

    return 'Bet is accepted';
  }

  @Authorized()
  @Query((returns) => GameEntity)
  public async getGame(
    @Args() {id}: GetGameDtoInput,
    @Ctx() ctx: UserContext,
  ) {
    const game = await this.service.repository.findOne({
      where: {
        id,
        user: ctx.user,
      }
    });

    if(!game) {
      throw new Error('Game does not exist');
    }

    return game;
  }

  @Authorized()
  @Subscription({
    topics: 'onGame',
    filter: ({ payload, context }) => (context && context.user && (context.user.id === payload.id)),
  })
  onGame(
    @Root() data: any,
  ): ValueGameDtoOutput {
    return data.game;
  }
}
