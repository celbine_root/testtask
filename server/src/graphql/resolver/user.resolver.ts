import { LogInDtoInput, LogInDtoOutput, SignUpDtoInput, SignUpDtoOutput, UserService } from '../../service';
import { Args, Authorized, Ctx, Mutation, Query, Resolver, Root, Subscription } from 'type-graphql';
import { UserEntity } from '../../database/entity';
import { UserContext } from '../../type';
import { Utils } from '../../tool';

@Resolver((of) => UserEntity)
export class UserResolver {
  service: UserService;

  constructor() {
    this.service = new UserService();
  }

  @Mutation((returns) => SignUpDtoOutput)
  public async signUp(
    @Args() dto: SignUpDtoInput,
    @Ctx() ctx: UserContext,
  ) {
    return this.service.signUp(dto);
  }

  @Mutation((returns) => LogInDtoOutput)
  public async logIn(
    @Args() dto: LogInDtoInput,
    @Ctx() ctx: UserContext,
  ) {
    return this.service.logIn(dto);
  }

  @Authorized()
  @Query((returns) => UserEntity)
  public async getMe(
    @Ctx() ctx: UserContext,
  ) {
    return this.service.getMe(ctx);
  }

  @Authorized()
  @Subscription({
    topics: 'onBalance',
    filter: ({ payload, context }) => (context && context.user && (context.user.id === payload.id)),
  })
  onBalance(
    @Root() data: any,
  ): number {
    return Utils.ToDigits(data.balance);
  }
}
