import { GraphQLSchema } from 'graphql';
import { ApolloServer as _ApolloServer } from 'apollo-server-express';
import { verify } from 'jsonwebtoken';
import { getCustomRepository } from 'typeorm';
import { UserRepository } from '../database/repository';
import { UserEntity } from '../database/entity';
import { Serializer } from '../database/serialization/serializer';

export class ApolloServer {
  serializer: Serializer;

  constructor() {
    this.serializer = new Serializer();
  }

  get(schema: GraphQLSchema): _ApolloServer {
    return new _ApolloServer({
      schema,
      context: async ({req, res, connection}) => {
        try {
          if (!req) {
            return connection && connection.context;
          }

          const authHeader = req.headers.authorization || null;

          if (!authHeader) {
            return;
          }

          const [, token] = authHeader.split(' ');

          if (!token) {
            return;
          }

          const payload = verify(token, process.env.JWT_SECRET) as Partial<UserEntity>;

          const user = await getCustomRepository(UserRepository).getUserById(payload.id);

          if (!user) {
            return;
          }

          return {user: {...this.serializer._serializeUser(user)}, header: req.headers};
        } catch (e) {
          return;
        }
      },
      subscriptions: {
        onConnect: async (connectionParams, websocket, context) => {
          try {
            const authHeader = (connectionParams as any).Authorization || null;

            if (!authHeader) {
              return;
            }

            const [, token] = authHeader.split(' ');

            if (!token) {
              return;
            }

            const payload = verify(token, process.env.JWT_SECRET) as Partial<UserEntity>;

            const user = await getCustomRepository(UserRepository).getUserById(payload.id);

            if (!user) {
              return;
            }

            return {user: {...this.serializer._serializeUser(user)}, header: connectionParams};
          } catch (e) {
            return;
          }
        }
      },
      tracing: true,
      cacheControl: true,
      introspection: true,
      playground: true,
    })
  }
}
