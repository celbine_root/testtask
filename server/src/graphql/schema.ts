import { buildSchema, MiddlewareFn } from 'type-graphql';
import { pubSub } from './pubsub';
import { customAuthChecker } from './auth.checker';
import { GameResolver, UserResolver } from './resolver';

const ErrorInterceptor: MiddlewareFn<any> = async ({ context, info }, next) => {
  try {
    return await next();
  } catch (error) {
    if (
      error.validationErrors
      && error.validationErrors[0]
      && error.validationErrors[0].constraints
      && Object.keys(error.validationErrors[0].constraints).length > 0
    ) {
      throw new Error(Object.values(error.validationErrors[0].constraints)[0] as string);
    }

    throw error;
  }
};

export const createSchema = () => buildSchema({
  resolvers: [UserResolver, GameResolver],
  pubSub,
  authChecker: customAuthChecker,
  validate: true,
  globalMiddlewares: [ErrorInterceptor],
});
