import { RedisPubSub } from 'graphql-redis-subscriptions';
import * as Redis from 'ioredis';

export const pubSub = new RedisPubSub({
  publisher: new Redis(process.env.REDIS_URL),
  subscriber: new Redis(process.env.REDIS_URL),
});
