import {join} from "path";
import {config} from "dotenv";
import { NodeEnv } from '../type';

const env = process.env.NODE_ENV as NodeEnv || 'development';

const envConfigs: Record<NodeEnv, string> = {
  development: "dev.env",
  production: "prod.env",
};



config({
  path: join(process.cwd(), "env", envConfigs[env]),
});
