import { UserEntity } from '../entity';
import { EntityRepository, FindOneOptions, getCustomRepository, Repository } from 'typeorm';
import { CreateUserDtoInput } from '../../service';
import { GamesRepository } from './games.repository';

@EntityRepository(UserEntity)
export class UserRepository extends Repository<UserEntity> {
  async createNewUser(dto: CreateUserDtoInput): Promise<UserEntity> {
    const newUser = Object.assign(new UserEntity(), dto);
    const saved = await this.save(newUser);
    return this.getUserById(saved.id);
  }

  async getUserById(id: number): Promise<UserEntity | undefined> {
    const opts: FindOneOptions<UserEntity> = {
      where: {
        id,
      },
    };

    const user = await this.findOne(opts);

    if (!user) {
      throw new Error('User does not exist');
    }

    user.games = await getCustomRepository(GamesRepository).getLastGamesByUserId(user.id);

    return user;
  }
}
