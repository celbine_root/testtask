import { DiceEntity } from '../entity';
import { EntityRepository, FindOneOptions, getCustomRepository, Repository } from 'typeorm';

@EntityRepository(DiceEntity)
export class DiceRepository extends Repository<DiceEntity> {
}
