import { EntityRepository, Repository } from 'typeorm';
import { GameEntity } from '../entity';
import { CreateGameDtoInput, } from '../../service';

@EntityRepository(GameEntity)
export class GamesRepository extends Repository<GameEntity> {
  async getLastGamesByUserId(id: number) {
    return this.createQueryBuilder()
      .where('user_id = :id', {id})
      .take(20)
      .orderBy('created_at', 'DESC')
      .getMany();
  }

  async createGame(dto: CreateGameDtoInput): Promise<GameEntity> {
    const newGame = Object.assign(new GameEntity(), dto);
    const saved = await this.save(newGame);
    return this.findOne(saved.id);
  }
}
