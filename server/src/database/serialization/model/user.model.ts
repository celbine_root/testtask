import { GameEntity } from '../../entity';

export class UserModel{
  constructor(
    public id: number,
    public username: string,
    public balance: number,
    public games: GameEntity[],
  ) {
  }

  static create(
   id: number,
   username: string,
   balance: number,
   games: GameEntity[],
  ): UserModel {
    return new UserModel(
      id,
      username,
      balance,
      games,
    );
  }
}
