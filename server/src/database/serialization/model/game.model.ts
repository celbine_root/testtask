import { GameEntity } from '../../entity';
import { GameEnum } from '../../../type';

export class GameModel {
  constructor(
    public game: GameEnum,
    public gameId: number,
    public amount: number,
    public winAmount: number,
  ) {
  }

  static create(
   game: GameEnum,
   gameId: number,
   amount: number,
   winAmount: number,
  ): GameModel {
    return new GameModel(
      game,
      gameId,
      amount,
      winAmount,
    );
  }
}
