import { GameEntity, UserEntity } from '../entity';
import { GameModel, UserModel } from './model';
import { Utils } from '../../tool';

export class Serializer {

  _serializeUser = (e: UserEntity) => {
    return UserModel.create(
      e.id,
      e.username,
      Utils.ToDigits(e.balance),
      e.games
    );
  };

  _serializeGame = (e: GameEntity) => {
    return GameModel.create(
      e.game,
      e.gameId,
      Utils.ToDigits(e.amount),
      Utils.ToDigits(e.winAmount)
    );
  };
}
