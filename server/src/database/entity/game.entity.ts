import { CustomBaseEntity } from './base.entity';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { GameEnum } from '../../type';
import { UserEntity } from './user.entity';
import { Field, Float, Int, ObjectType, registerEnumType } from 'type-graphql';
import { Utils } from '../../tool';

registerEnumType(GameEnum, { name: 'GAME' });

@ObjectType()
@Entity('game')
export class GameEntity extends CustomBaseEntity {
  @Field((type) => GameEnum)
  @Column('enum', {nullable: false, name: 'game', enum: GameEnum})
  game: GameEnum;

  @Field((type) => Int)
  @Column({
    name: 'game_id',
  })
  gameId: number;

  @Field((type) => Float)
  @Column('bigint', {
    default: 0,
    transformer: {
      to: Utils.ToBigInt,
      from: Utils.FromBigInt,
    },
    nullable: false
  })
  amount: number;

  @Field((type) => Float)
  @Column('bigint', {
    default: 0,
    name: 'win_amount',
    transformer: {
      to: Utils.ToBigInt,
      from: Utils.FromBigInt,
    },
    nullable: false
  })
  winAmount: number;

  @ManyToOne((type) => UserEntity, (user) => user.games)
  @JoinColumn({
    name: 'user_id',
    referencedColumnName: 'id',
  })
  user: UserEntity;
}
