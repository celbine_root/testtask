import "reflect-metadata";
import "./tool/parse-dot-env";
import { createConnection } from './database/connection';
import { ServerRun } from './server';

(async function () {
  await createConnection();

  const server = new ServerRun(9999);
  await server.start();
})();
